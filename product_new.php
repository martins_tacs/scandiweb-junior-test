<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="scandi_test.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>



<body>

<!--NAVBAR-->
<div class="container">
<nav class="menu_bar">
  <div class="container-fluid">
    <div class="navbar-header">
      <h1>Product Add</h1>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li>
      <div class="submit_form">
      <form action="" method="post">
   
<input class="submit_button" type="submit" name = "save" value="Save">
</div>
      </li>
    </ul>
  </div>
</nav>       
<!--NAVBAR-->

<?php
//
//CONNECTION TO DATABASE
//
$servername = "localhost";
$username = "root";
$password = "tacs";
$dbname = "product";
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
//
//CONNECTION TO DATABASE
//

if(isset($_POST['save']))
{
       $sku = $_POST['sku'];
       $product_name = $_POST['product_name'];
       $price = $_POST['price'];
       $size = $_POST['size'];
       $book_weight = $_POST['book_weight'];
       $height = $_POST['height'];
       $width = $_POST['width'];
       $f_length = $_POST['f_length'];

       $selectOption = false;

if(isset($_POST['product_type']))
{
    $selectOption = $_POST['product_type'];
} 
if( $selectOption == "")
 {

   echo "<script type='text/javascript'>alert('There is no type selected!');</script>";

 }else{
    if( $selectOption == "disc")
    {
        add_disc();
     }
    else if( $selectOption == "book")
    {  
        add_book();
     }
    else if( $selectOption == "furniture")
    {
        add_furniture();
     }
      }
}

    function add_disc()
    {
        $sku = $GLOBALS['sku'];
        $product_name = $GLOBALS['product_name'];
        $price = $GLOBALS['price'];
        $size = $GLOBALS['size'];
          
        $sql = "INSERT INTO disc (sku, product_name, price, size)
        VALUES ('$sku', '$product_name', '$price', '$size')";

        if ($GLOBALS['conn']->query($sql) === TRUE){
          echo "<script type='text/javascript'>alert('New record created successfully');</script>";
        }else{
          echo "Error: " . $sql . "<br>" . $GLOBALS['conn']->error;
        }   
    }

    function add_book()
    {
        $sku = $GLOBALS['sku'];
        $product_name = $GLOBALS['product_name'];
        $price = $GLOBALS['price'];
        $book_weight = $GLOBALS['book_weight'];
          
        $sql = "INSERT INTO book (sku, product_name, price, book_weight)
        VALUES ('$sku', '$product_name', '$price', '$book_weight')";
      
        if ($GLOBALS['conn']->query($sql) === TRUE) {
          echo "<script type='text/javascript'>alert('New record created successfully');</script>";
        }else{
          echo "Error: " . $sql . "<br>" . $GLOBALS['conn']->error;
        }   
    }

    function add_furniture()
    {
        $sku = $GLOBALS['sku'];
        $product_name = $GLOBALS['product_name'];
        $price = $GLOBALS['price'];
        $height = $GLOBALS['height'];
        $width = $GLOBALS['width'];
        $f_length = $GLOBALS['f_length'];
     
        $sql = "INSERT INTO furniture (sku, product_name, price, height, width, f_length)
        VALUES ('$sku', '$product_name', '$price', '$height', $width, $f_length)";

        if ($GLOBALS['conn']->query($sql) === TRUE) {
          echo "<script type='text/javascript'>alert('New record created successfully');</script>";
        }else{
            echo "Error: " . $sql . "<br>" . $GLOBALS['conn']->error;
        }   
    }
?>

<br> 
<div class="row">
<div class="col-25">
  <label for="sku">SKU</label>
</div>
<div class="col-75">
  <input type="text" id="sku" name="sku" value="" required>
</div>
</div>

<div class="row">
<div class="col-25">
  <label for="name">Name</label>
</div>
<div class="col-75">
  <input type="text" id="product_name" name="product_name" value="" required>
</div>
</div>

<div class="row">
<div class="col-25">
  <label for="price">Price</label>
</div>
<div class="col-75">
  <input type="text" id="price" name="price" value="" pattern= "\d+(.\d{2})?" title="Format: only numbers, divided by '.' " required>
</div>
</div>



<div class="row">
<div class="col-25">
<label for="sku">Type Switcher</label>
</div>
<div class="col-75">
<select required name="product_type" id="product_type" onchange="ShowDiv(this)">
<option value="" selected="selected" disabled>Select one option </option>
<option value="disc">Disc</option>
<option value="book">Book</option>
<option value="furniture">Furniture</option>
</select>
</div>
</div>



<br>

<div style="display:none" id="disc">
<div class="row">
<div class="col-25">
  <label for="size">Size</label>
</div>
<div class="col-75">
  <input required=false type="text" id="size" name="size" value="">
  <p>Please provide size in Mb!</p>
</div>
</div>
</div>

<div style="display:none" id="book">
<div class="row">
<div class="col-25">
  <label for="length">Weight</label>
</div>
<div class="col-75">
  <input required=false type="text" id="book_weight" name="book_weight" value="">
  <p>Please provide weight in Kg!</p>
</div>
</div>
</div>


<div style="display:none" id="furniture">
<div class="row">
<div class="col-25">
  <label for="price">Height</label>
</div>
<div class="col-75">
  <input required=false type="text" id="height" name="height" value="">
</div>
</div>
<div class="row">
<div class="col-25">
  <label for="width">Width</label>
</div>
<div class="col-75">
  <input required=false type="text" id="width" name="width" value="">
</div>
</div>
<div class="row">
<div class="col-25">
  <label for="f_length">Length</label>
</div>
<div class="col-75">
  <input required=false type="text" id="f_length" name="f_length" value="">
  <p>Please provide dimensions in HxWxL (Cm) format!</p>
</div>
</div>

</form> 


<script>

function ShowDiv(el)
{
    if(el.value == "disc")
    {
        document.getElementById('disc').style.display = "block";
        document.getElementById('book').style.display = "none";
        document.getElementById('furniture').style.display = "none";
   
        document.getElementById("size").required = true;
        document.getElementById("book_weight").required = false;
        document.getElementById("height").required = false;
        document.getElementById("width").required = false;
        document.getElementById("f_length").required = false;
    }else if(el.value == "book")
    {
        document.getElementById('disc').style.display = "none";
        document.getElementById('book').style.display = "block";
        document.getElementById('furniture').style.display = "none";

        document.getElementById("size").required = false;
        document.getElementById("book_weight").required = true;
        document.getElementById("height").required = false;
        document.getElementById("width").required = false;
        document.getElementById("f_length").required = false;
    }else if(el.value == "furniture")
    {
        document.getElementById('disc').style.display = "none";
        document.getElementById('book').style.display = "none";
        document.getElementById('furniture').style.display = "block";

        document.getElementById("size").required = false;
        document.getElementById("book_weight").required = false;
        document.getElementById("height").required = true;
        document.getElementById("width").required = true;
        document.getElementById("f_length").required = true;
    }
}

</script>

</body>
</html>