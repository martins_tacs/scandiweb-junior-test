<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="scandi_test.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>



<body>

<!--NAVBAR-->
<div class="container">
<nav class="menu_bar">
  <div class="container-fluid">
    <div class="navbar-header">
      <h1>Product List</h1>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li>
     <div class="submit_form"> <form name="submit_form" method="post">
<select class="select_dropdown" name="action_type">
  <option value="delete">Mass Delete Action</option>
  <option value="other">Other Action</option>
</select>
<input class="submit_button" type="submit" name="submit_product_page" value="Submit">
</div>
      </li>
    </ul>
  </div>
</nav>       
<!--NAVBAR-->




<?php 
//
//CONNECTION TO DATABASE
//
$servername = "localhost";
$username = "root";
$password = "tacs";
$dbname = "product";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} 
//
//CONNECTION TO DATABASE
//

$sql_disc = "SELECT * FROM disc";
$sql_book = "SELECT * FROM book";
$sql_furniture = "SELECT * FROM furniture";
$result_disc = $conn->query($sql_disc);
$result_book = $conn->query($sql_book);
$result_furniture = $conn->query($sql_furniture);

// Return the number of rows
$rowcount_disc=mysqli_num_rows($result_disc);
$rowcount_book=mysqli_num_rows($result_book);
$rowcount_furniture=mysqli_num_rows($result_furniture);
$rowcount_result = $rowcount_disc + $rowcount_book + $rowcount_furniture;
  
if($rowcount_result > 0){
  if ($result_disc->num_rows > 0) {
    while($row = $result_disc->fetch_assoc()) {
   
       echo  ' <div class="productbox">
       <input type="checkbox" name="id_arr_disc[]" value="'.$row["id"].'">
  <center>
       <div class="productsku">'.$row["sku"].'</div>
       <div class="productname">'.$row["product_name"].'</div>
       <div class="">
       <div class="productprice">'.$row["price"].' $</div></div>
       <div class="productattribute">Size: '.$row["size"].' MB</div>
  </center>
       </div>' ;
   
      }
  }
  
  if ($result_book->num_rows > 0) {
    while($row = $result_book->fetch_assoc()) {
   
       echo  ' <div class="productbox ">
       <input type="checkbox" name="id_arr_book[]" value="'.$row["id"].'">
  <center>
       <div class="productsku">'.$row["sku"].'</div>
       <div class="productname">'.$row["product_name"].'</div>
       <div class="">
       <div class="productprice">'.$row["price"].' $</div></div>
       <div class="productattribute">Weight: '.$row["book_weight"].' KG</div>
  </center>
       </div>' ;
      }
  }
  
  if ($result_furniture->num_rows > 0) {
     while($row = $result_furniture->fetch_assoc()) {
      
       echo  ' <div class="productbox">
       <input type="checkbox" name="id_arr_furniture[]" value="'.$row["id"].'">
  <center>
       <div class="productsku">'.$row["sku"].'</div>
       <div class="productname">'.$row["product_name"].'</div>
       <div class="">
       <div class="productprice">'.$row["price"].' $</div></div>
       <div class="productattribute">Dimension: '.$row["height"].'x'.$row["width"].'x'.$row["f_length"].'</div>
  </center>
       </div>' ;
      }
  }

mysqli_free_result($result_disc);
mysqli_free_result($result_book);
mysqli_free_result($result_furniture);

}else{
  echo "<h1>There are no products in database!</h1>";
}


?>
</form>
<?php
if(isset($_POST["submit_product_page"]))
{
  $selected_action = $_POST['action_type'];
  if($selected_action == "delete")
  {
    if(!isset($_POST['id_arr_disc']) && !isset($_POST['id_arr_book']) && !isset($_POST['id_arr_furniture']))
    {
      echo "<script type='text/javascript'>alert('There is no product selected!');</script>";
    }else{
      //header("Location: ".$_SERVER['HTTP_REFERER']);
      if(isset($_POST['id_arr_disc']))
      {
       foreach($_POST['id_arr_disc'] as $id):
         mysqli_query($conn, "delete from disc where id='$id'");
       endforeach;
      }
      if(isset($_POST['id_arr_book']))
      {
       foreach($_POST['id_arr_book'] as $id):
         mysqli_query($conn, "delete from book where id='$id'");
       endforeach;
      }
      if(isset($_POST['id_arr_furniture']))
      {
       foreach($_POST['id_arr_furniture'] as $id):
         mysqli_query($conn, "delete from furniture where id='$id'");
       endforeach;
      }
      echo "<script> location.replace('product_page.php'); </script>";
    }
  }else if($selected_action == "other"){
    echo "<script type='text/javascript'>alert('Have a nice day! :)');</script>";
  }
}
?>
</div>
</body>
</html>